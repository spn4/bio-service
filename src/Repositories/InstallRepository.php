<?php

namespace SpondonIt\BioService\Repositories;
ini_set('max_execution_time', -1);

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Schema;
use SpondonIt\Service\Repositories\InstallRepository as ServiceInstallRepository;
use Modules\FrontEndCMS\Entities\FrontMenu;
use App\Models\GeneralSetting;

class InstallRepository {

    protected $installRepository;
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct(ServiceInstallRepository $installRepository) {
        $this->installRepository = $installRepository;
    }



    /**
     * Install the script
     */
    public function install($params) {

        try{
            $admin = $this->makeAdmin($params);
            
            $this->installRepository->seed(gbv($params, 'seed'));
            $this->postInstallScript($admin, $params);

            Artisan::call('key:generate', ['--force' => true]);

            envu([
                'APP_ENV' => 'production',
                'APP_DEBUG'     =>  'false',
            ]);



        } catch(\Exception $e){

            Storage::delete(['.user_email', '.user_pass']);

            throw ValidationException::withMessages(['message' => $e->getMessage()]);

        }
    }

    public function postInstallScript($admin, $params){
        
        $this->seedMenu();

        // Update general setting 
        $settings = GeneralSetting::find(1);
        $settings->system_activated_date = date('Y-m-d');
        $settings->system_domain = app_url();
        $settings->save();

    }


    public function seedMenu()
    {
            $i = 1;
            FrontMenu::create([
                'name' => 'Home',
                'url' => '/',
                'order_id' => $i++,
                'is_default' => 1,
            ]);

            FrontMenu::create([
                'name' => 'Popular',
                'url' => 'popular',
                'order_id' => $i++,
                'is_default' => 1,
            ]);

            FrontMenu::create([
                'name' => 'Recent',
                'url' => 'recent',
                'order_id' => $i++,
                'is_default' => 1,
            ]);

            FrontMenu::create([
                'name' => 'Designer',
                'url' => 'designers',
                'order_id' => $i++,
                'is_default' => 1,
            ]);

            FrontMenu::create([
                'name' => 'Teams',
                'url' => 'teams',
                'order_id' => $i++,
                'is_default' => 1,
            ]);

            FrontMenu::create([
                'name' => 'Membership',
                'url' => 'membership',
                'order_id' => $i++,
                'is_default' => 1,
            ]);

    }




    /**
     * Insert default admin details
     */
    public function makeAdmin($params) {
        try{
            $user_model_name = config('spondonit.user_model');
            $user_class = new $user_model_name;
            $user = $user_class->find(1);
            if(!$user){
               $user = new $user_model_name;
            }
            $user->name = 'Super admin';
            $user->email = gv($params, 'email');
            if(Schema::hasColumn('admins', 'role_id')){
                $user->role_id = 1;
            }
            if(\Illuminate\Support\Facades\Config::get('app.app_sync')){
                $user->password = bcrypt(12345678);

            }else{
                $user->password = bcrypt(gv($params, 'password', 'abcd1234'));
            }

            $user->save();
        } catch(\Exception $e){
            $this->installRepository->rollbackDb();
            throw ValidationException::withMessages(['message' => $e->getMessage()]);
        }


    }

}

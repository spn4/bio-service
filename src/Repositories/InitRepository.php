<?php

namespace SpondonIt\BioService\Repositories;

use App\Models\AdminStyle;
use App\Models\BackgroundSetting;
use App\Models\GeneralSetting;
use App\Models\SeoSetting;
use Illuminate\Support\ServiceProvider;
use stdClass;
use Illuminate\Support\Facades\Schema;
use Modules\RolePermission\Entities\Permission;
use Modules\RolePermission\Entities\Role;
use Modules\FrontEndCMS\Entities\SocialIcon;
use Illuminate\Support\Facades\Auth;
use Modules\FrontEndCMS\Entities\FooterCategory;
use Modules\FrontEndCMS\Entities\FrontMenu;
use App\Helpers\Browser;
use App\Helpers\VisitLog;
use Illuminate\Support\Facades\App;
use App\Models\TeamUser;
use Illuminate\Support\Facades\View;

class InitRepository {

    public function init() {
        config([
            'app.item' => '31733970',
            'spondonit.module_manager_model' => \App\InfixModuleManager::class,
            'spondonit.module_manager_table' => 'infix_module_managers',

            'spondonit.settings_model' => \App\Models\GeneralSetting::class,
            'spondonit.module_model' => \Nwidart\Modules\Facades\Module::class,

            'spondonit.user_model' => \App\Admin::class,
            'spondonit.settings_table' => 'general_settings',
            'spondonit.database_file' => '',
            'spondonit.support_multi_connection' => false,
            'spondonit.php_version' => '8.2.0'
        ]);
    }

    public function config()
    {
        if(Schema::hasTable('general_settings')){
           app()->singleton('login_background', function ($app,$parameters) {
                return BackgroundSetting::where([['is_default',$parameters['status']],['title','Login Background']])->first();
            });

           app()->singleton('dashboard_background', function($app) {
                return BackgroundSetting::where([['is_default',1],['title','Dashboard Background']])->first();
            });

           app()->singleton('admin_active_style', function ($app) {
                return AdminStyle::where('is_active',1)->first();
            });

           app()->singleton('seo', function() {
                return SeoSetting::find(1);
            });

           app()->singleton('permission_list', function() {
                return Role::with(['permissions' => function($query){
                    $query->select('permissions.id as per_id','route','module_id','parent_id','role_permission.role_id');
                }])->get(['id','name']);
            });

            $browser = new Browser();

            // This helps Facade resolve the actual class
           app()->bind('visitLog', static function () use ($browser) {
                return new VisitLog($browser);
            });

       

        
            $settings = GeneralSetting::first();
            $icons =  SocialIcon::where('status',1)->get();
            config([
                'settings' =>   $settings, // make it an array
                'icons' => $icons,
                'footers'=> FooterCategory::with('contents')->get(),
                'menus'=> FrontMenu::where('status', 1)->orderBy('order_id','asc')->get()
            ]);
            date_default_timezone_set($settings ? $settings->time_zone : 'Asia/Dhaka');
            config()->set('timezone',$settings ? $settings->time_zone : 'Asia/Dhaka');
            config()->set('locale',$settings ? $settings->language : 'en');
       


            View::composer('*', function ($view) {

                $is_user_team_owner = '';
                if (Auth::check()) {
                    $is_team = TeamUser::with('user')->where("user_id", auth()->user()->id)
                        ->where('status', 1)->first();
                    $is_user_team_owner = isset($is_team) ? $is_team : '';
                }
                $view->with('is_user_team_owner', $is_user_team_owner);
            });
        
        }

    }

}

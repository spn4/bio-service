<?php

namespace SpondonIt\BioService;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use SpondonIt\BioService\Middleware\BioService;

class SpondonItBioServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(BioService::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'bio');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'bio');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
